import ButtonPlayAudio from './js/button-play-audio.js'

customElements.define('button-play-audio', ButtonPlayAudio)

export {
	/* custom elements */
	ButtonPlayAudio,
}
