> Original idea by [Hagström
> FitzPatrick](https://www.hagstromfitzpatrick.com) and inspired by
> [plan8's soundbricks](https://plan8.se/tools/soundbricks/).

# button-play-audio

Am HTML button, web component `<button-play-audio/>`, to play an audio file from a `url`, and optionally display a `text` and visual **icon** (google `material-symbols`, `font-awesome` icons, `lottie-player` animation), which can be animated (using font-awesome animations, or custom CSS & variables).

- [git](https://gitlab.com/publicservices/web-components/button-play-audio)
- [npm](https://www.npmjs.com/package/@publicservices/button-play-audio)
- [cdn](https://www.jsdelivr.com/package/npm/@publicservices/button-play-audio)

> Right now in early alpha version, use "the button" at your own risk, and maybe don't customize too much as this web components API might as well change for now (wait for version 1.0.0)

## usage

1. import the javascript "once" in the website (across all pages you'd like the element to be available)
```html
<script type="module" src="https://cdn.jsdelivr.net/npm/@publicservices/button-play-audio@latest/index.js"></script>
```

> Insert the HTML `script` element above, into the HTML of your website. Either only on the page where youd like to also insert our new button, or in a place loaded by every pages of your site. You can insert it before the closing `</body>` tag, so it is loaded after all other assets of your site, or before the closing `</head>` tag, so it loads before.

Alternatively, import the same script, from inside a javascript file of your site.

```javascript
import('https://cdn.jsdelivr.net/npm/@publicservices/button-play-audio@latest/index.js')
```

> Be sure to only import the script once per page (best is once on all the site, for example is your site's "layout" or "header/footer injection").

> the `@latest` part in the URL of the script, is the version number
> of this custom button. You can replace it by `@0.0.10` (don't prefix
> by `v` as on gitlab) or any other version number, if you want to be
> sure not to get the latest upgrades of the button, so it might not
> break your site, as the functionalities of the button might evolve
> with the needs of the users. You can find the full list of version
> numbers on the gitlab project's repository or on npm. A best
> practise, is to "ping" the latest available version number, if it
> works as you wish, and update to newer versions manually, while
> testing that every buttons still behave as expected.

2. add a `button-play-audio` element, in the HTML of your website, in the places you would like it to appear (it could also go in markdown, depending om your website's system and DOM serialization rules).

```html
<button-play-audio
	url="https://upload.wikimedia.org/wikipedia/commons/c/c4/Drum_Roll_Intro.ogg"
	text="click me"
	title="This text appears when hovering with the mouse"
></button-play-audio>
```

> You can use the HTML element as many times as needed in your website. Customize the attributes `url` and `text` so the button displays what you want it to.

## example of the button features

It is possible to add more customization to the button; checkout this current document for reference, as well as the `./index.html` file for more examples (our example webpage).

> Use only "one type of icon" and/or "one type of text", by attributes or slot, for each button.

- [simplest example usage](https://jsbin.com/funeyohala/1/edit?html,output)
- [importing and using icons](https://jsbin.com/xaselalanu/edit?html,output)
- [using styles](https://jsbin.com/derapuroru/1/edit?html,css,output)

### Supported audio file types and `url`

You can use any file encoded in a format with an audio codec supported by modern web browsers, [see the MDN guide](https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Audio_codecs) for a full reference.

You should use a URL pointing to an `audio` file accessible publicliy accessible, [such as this one](https://upload.wikimedia.org/wikipedia/commons/b/b8/Mystery_mystery_-_Identity_unknown_XC302835.mp3).

> If your sound file (and URL) work with the [HTML `audio` embed element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/audio), it should work with this button

> Try to use "small" audio files for your button, in term of file size, so it loads fast and feels right while clicking the button. Also the button is not entended for "long duration" audio files, but more for short sounds used for web interactions, (like the sound of "opening a door").

### Customizing styles

There are various methods to customizing the styles of the button using CSS and HTML.

#### With a `style` attribute on the button

Directy on the `style` attribute of the button, using CSS and the variables available, to make them specific to this button only.

```html
<button-play-audio
	style="--color-text: hotpink; --color-background: purple;"
	url="https://upload.wikimedia.org/wikipedia/commons/9/9e/Snare_sem_reverb.ogg"
	text="HOT">
</button-play-audio>
```

#### With a `type` attribute on the button

As an alternative to using regular CSS classes, you can also use attributes to customize the button styles., so it they can be shared among buttons having this "type".

```html
<button-play-audio
	type="featured"
	url="https://upload.wikimedia.org/wikipedia/commons/9/9e/Snare_sem_reverb.ogg"
	text="Tsch🥁kk">
</button-play-audio>
```

Then in place of your site where you can write CSS, insert the following code:
```css
butotn-play-audio {
	padding: 1rem;
	margin: 0.5rem;
}
button-play-audio[type="featured"] {
	--color-text: red;
	--color-background: blue;
	--size-font: 2rem; /* could also be px (pixel), `em` or `rem` are proportional */
}
```

All `button-play-audio` HTML elements, with the attribute `type` set to the value `featured`, will now inherit these styles. If you remove the `[type="featured"]` part in the CSS declaration above, all of the `<button-play-audio/>` will be targeted.

- for a full list of available CSS variables to customize the button look and feel, see the `./styles/button-play-audio.css` file.
- for more examples on how to make "button types", see the file `./styles/button-play-audio-types.css`

#### Button CSS variables and state attributes

All availables CSS variables and their default values:

```css
button-play-audio {
	--font-family: inherit;
	--size: 1.5rem;
	--size-font: var(--size-font, 17px);
	--size-icon: 1.5em;
	--color-text: black;
	--color-background: whitesmoke;
	--color-border: lightgray;
	--color-icon: var(--color-text);
	--color-loading: var(--color-text);
	--color-play: whitesmoke;
	--border-radius: 0.4em;
	--transition-duration: 111ms;
	--transition-timing: ease-in-out;
}
```

All available states of the buttons, via its attributes (open the developper tools, and inspect a `button-play-audio` HTML element in a webpage, click on it, see how its attribute change while the audio plays and ends).

```css
button-play-audio {
	--transition-duration: 555ms;
}
button-play-audio[loaded="true"] {
	--color-border: green;
}
button-play-audio:not([loaded="true"]) {
	--color-border: red;
}

button-play-audio[play="true"] {
	--color-background: hotpink;
}
```

> Check out the `./styles` folder for more examples and complete list of attributes.

### google material symbols typeface

You can use Google's [Material
Symbols](https://fonts.google.com/icons?icon.platform=web&icon.set=Material+Symbols)
(not material icons) font to your site, any of the icons, inside the
button.

> Do not copy the following code; use the one provided by google's
> font website after you have customized how your material symbols
> font should look like.

- **important**: you will need to add the `button-play-audio` attribute on the `link` HTML tag that you've copied from google's site (see example bellow) That way the buttons will know about the font'sstyles that you've imported (do not do that for other `link` styles, not related to using "icons' font" in this audio button).

```html
<!-- google material symbols typeface -->
<link button-play-audio rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Rounded:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />

<!-- google material symbols configuration -->
<style>
	.material-symbols-rounded {
		font-variation-settings:
		'FILL' 1,
		'wght' 300,
		'GRAD' 200,
		'opsz' 48
 }
</style>
```

> Import *once* the "font's stylesheet" in your website (as children
> of the "header" HTML element), and declare your personal customized
> configuration (as chosen on the website).

> update the HTML `<style>` tag with the new values for the
> `.material-symbols-rounded` class, when you change them in google
> font's customization page

> Be sure to check for the "type" of the font you're using `Outlined`,
> `Rounded`, `Sharp`; and that the one you import, the one you use in the style tag and on the button, are the same (or that you import all the ones you need).

Now it is possible to use the font, by inserting the selected icon
into the HTML markup of your website, in place where you need it.

```html
<!-- normally in any other page -->
<span class="material-symbols-outlined">grass</span>

<!-- (example) in the `slot` of a button-play-audio element, note the `slot` attribute -->
<button-play-audio url="https://upload.wikimedia.org/wikipedia/commons/7/79/Dropping_rolled_up_water_hose_%28Gravity_Sound%29.wav">
	<span slot="icon" class="material-symbols-outlined">grass</span>
</button-play-audio>
```

As a convenience, the same result can be achieved with the buttons' `symbol` attribute (for *google material symbols* library), without the need of a child element and a `slot` attribute.

```html
<!-- with a `symbol` attribute on a `button-play-audio` element -->
<button-play-audio symbol="grass"
	url="https://upload.wikimedia.org/wikipedia/commons/7/79/Dropping_rolled_up_water_hose_%28Gravity_Sound%29.wav"></button-play-audio>
```

and you can also pass (optionnally) the `symbol-type` as an attribute:

```html
<!-- with a `symbol` attribute on a `button-play-audio` element -->
<button-play-audio symbol="grass" symbol-type="sharp"
	url="https://upload.wikimedia.org/wikipedia/commons/7/79/Dropping_rolled_up_water_hose_%28Gravity_Sound%29.wav"></button-play-audio>
```

### font-awesome

To use [font awesome](https://fontawesome.com/search) icon library if icons, [import the library of your choice](https://cdnjs.com/libraries/font-awesome), inside the `head` element of your site (same as for material symbols).

- **important** you will need to add the `button-play-audio` attribute on the `link` HTML tag that you've copied from

```html
<!--
	we are inside the HTML Element `html > head`,
	remember to add the `button-play-audio` attribute;
	(it does not require any value)
-->
<link button-play-audio rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
```

Now it is possible to use the `fa=""` attribute on the button, to load a font-awesome icon. Use the `fa` attribute, with the same value of the icon's `class` attribute, provided by the font-awesome site (on the `i` element of the icon).

```html
<!-- using font-awesome normally in a site -->
<i class="fa-solid fa-check"></i>

<!-- using font awesome on our audio button -->
<button-play-audio fa="fa-solid fa-check"
	url="https://upload.wikimedia.org/wikipedia/commons/7/79/Dropping_rolled_up_water_hose_%28Gravity_Sound%29.wav"></button-play-audio>
```

> If some icons don't work, it might be a bug with our button, or it might be that you did not import the correct font-awesome css styles (be sure the icons you request is part of the icon kit your import).

#### using font awesome icon animations

> These animations can also be used with the `symbol` attribute, but might combine with imprevisible results.

It is possible to [animate font-awesome icons](https://fontawesome.com/v6/docs/web/style/animate). Add the CSS class for the animation to the `fa` attribute, with the CSS class for this icon.

```html
<button-play-audio fa="fa-solid fa-check fa-beat"
	url="https://upload.wikimedia.org/wikipedia/commons/7/79/Dropping_rolled_up_water_hose_%28Gravity_Sound%29.wav"></button-play-audio>
```

If you would like an animation to only be when the button is in `play` state (and the audio is playing), you can use the attribute `fa-play` (these classes will be removed when the audio is done playing).

```html
<button-play-audio
	fa="fa-solid fa-check"
	fa-play="fa-beat"
	url="https://upload.wikimedia.org/wikipedia/commons/7/79/Dropping_rolled_up_water_hose_%28Gravity_Sound%29.wav"></button-play-audio>
```

> To add CSS font-awesome CSS customization of the animations through CSS variables, check out how to customize the button styles in this document, and use the CSS variables in your "button types".

When [customizing the animations](https://fontawesome.com/v6/docs/web/style/animate#customization) with CSS variables, you can add the `style=""` tag like in the examples, but directly as attribute of the `button-play-audio` elements (if you prefer not to make a "button type").

```html
<button-play-audio fa="fa-solid fa-wind" fa-play="fa-flip" text="fa-wind" url="https://upload.wikimedia.org/wikipedia/commons/f/fd/De-Wind2.ogg" style="--fa-animation-duration: 3s;"></button-play-audio>
```

### lottie-player & animations

The `lottie-player` web-component ([attributes docs](https://docs.lottiefiles.com/lottie-player/components/lottie-player/properties)) can be used to have a lottie animation launched (and stopped) with the audio file being played. Use the `slot="icon` attribute on a `lottie-player` element, direct children of the `button-play-audio`.

```html
<button-play-audio text="lottie & sound" url="https://upload.wikimedia.org/wikipedia/commons/7/79/Dropping_rolled_up_water_hose_%28Gravity_Sound%29.wav">
	<lottie-player
		slot="icon"
		speed="3"
		controls="true"
		style="width: 10rem;"
		src="https://assets1.lottiefiles.com/datafiles/DJyJpOnuyK5DEaGtKEpWNPbjUjGJSMhr2sFgGaZS/AE_JSON/BT_WB.json">
	</lottie-player>
</button-play-audio>
```

> The attibutes for the `lottie-player` can be found in its documentation.

> When the audio is in state `ended`, the lottie animation is stopped (sets to its initial state, with the audio).

> The `lottie-player` element, can also be used as a **direct children** of a `<i slot="icon">`

### Note on using the *slots*

> Slots are an *advanced feature* (and require some knowledge of css/html/js), use icons & text without slots, except in cases you really need them.

A `slot`, tells the button component, where to insert its children element, in specific places its code knows about.

There are two slots we can use: `icon`, `text`, to fully customize the content of the button.

```html
<button-play-audio
	url="https://upload.wikimedia.org/wikipedia/en/8/8a/Ellen_comes_out_airport.mp3"
	text="Click me">
	<i slot="icon">💗</i>
</button-play-audio>
```

The `icon` slot, should be a `i` HTMLElement, and can have any icon (unicode, `svg`, `img`, `span` with fontawesome `fa-` classes etc.), and also `lottie-player` HTMLElement. It can as well be a `span`, in the case of material symbols.

The `text` slot, can be any HTMLElement, and can also have children, such as `details` and `summary`.

Alway only use **one** of each slot maximum; if you would like, for
example, to have multiple icons in the icon `slot`, you could do:

```html
<button-play-audio
	url="https://upload.wikimedia.org/wikipedia/commons/b/b8/Mystery_mystery_-_Identity_unknown_XC302835.mp3"
	text="play sound">
	<span slot="icon">
		<i>😍</i>
		<i>💌</i>
	</span>
</button-play-audio>
```

#### Hidden `slot="style"`

It is also possible to use a thirs hidden experimental slot, but its interest is yet to be defined/explored.

```html
<button-play-audio
	type="featured"
	url="https://upload.wikimedia.org/wikipedia/commons/9/9e/Snare_sem_reverb.ogg"
	text="click me">
	<style slot="style">
		:host {
			background-color: purple;
			/*
				does this work correcty, or brings any special interest?
				It might also remove the buttons' imported styles
				(see `constructor > this.loadStyles`)
			*/
		}
	</style>
</button-play-audio>
```

> use at your own risks

## Debug & issues

If something is unclear or not working, do not hesitate to open an issue on this gitlab repository. Otherwise this document should have all information needed to display all features, how to make them work and tweak them.
