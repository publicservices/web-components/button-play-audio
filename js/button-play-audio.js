/* the template of the widget */
const template = document.createElement('template')
template.innerHTML = `
	<button>
		<slot name="icon"></slot>
		<slot name="text"></slot>
		<slot name="style"></slot>
	</button>
`
export default class ButtonPlayAudio extends HTMLElement {
	static get observedAttributes() {
		return [
			'url', 'text',
			'symbol', 'symbol-type',
			'fa', 'fa-play',
		]
	}

	get url() {
		return this.getAttribute('url')
	}
	get text() {
		return this.getAttribute('text')
	}

	get symbol() {
		return this.getAttribute('symbol')
	}
	get symbolType() {
		const sType = this.getAttribute('symbol-type') || 'sharp'
		return sType.toLowerCase()
	}

	/* list of class="" for `fa` (font-awesome) */
	get fa() {
		const fontAwesomeClasses = this.getAttribute('fa')
		let classes = []
		if (fontAwesomeClasses) {
			classes = fontAwesomeClasses.split(' ')
		}
		return classes
	}
	get faPlay() {
		const faClasses = this.getAttribute('fa-play')
		let classes = []
		if (faClasses) {
			classes = faClasses.split(' ')
		}
		return classes
	}

	/* if the attribute changed, re-render */
	attributeChangedCallback(attrName) {
		if (
			[
				'url', 'text',
				'symbol', 'symbol-type',
				'fa',
			].indexOf(attrName) > -1
		) {
			this.render()
		}
	}

	constructor() {
		super()
		this.attachShadow({ mode: 'open' })
		this.shadowRoot.append(template.content.cloneNode(true))

		this.addEventListener('click', this.onClick.bind(this))

		/* widget container */
		this.$button = this.shadowRoot.querySelector('button')

		this.$icon = this.shadowRoot.querySelector('slot[name="icon"]')
		this.$text = this.shadowRoot.querySelector('slot[name="text"]')
		this.$styles = this.shadowRoot.querySelector('slot[name="style"]')

		this.loadStyles()
	}
	loadStyles() {
		const urlPaths = import.meta.url.split('/')
		const stylesUrl = urlPaths.slice(0, urlPaths.length - 2).join('/')
		const styleImportCSS = `@import "${stylesUrl}/styles/button-play-audio.css";`

		const $buttonStyles = document.createElement('style')
		$buttonStyles.innerText = styleImportCSS
		this.$styles.append($buttonStyles)

		/* duplicate the reference to all icons styles, if icons used */
		const $allIconsStyles = document.querySelectorAll('link[button-play-audio]')
		if ($allIconsStyles && $allIconsStyles.length) {
			$allIconsStyles.forEach($iconsStyles => {
				this.$styles.append($iconsStyles.cloneNode())
			})
		}
	}

	connectedCallback() {
		this.setAttribute('disabled', true)
		/* create audio */
		this.audio = new Audio(this.url)
		this.audio.loop = false

		/* attach listeners */
		this.audio.addEventListener('loadeddata', this.handleLoadedData.bind(this))
		this.audio.addEventListener('play', this.handlePlay.bind(this))
		this.audio.addEventListener('ended', this.handleEnded.bind(this))
		this.audio.addEventListener('error', this.handleError.bind(this))
		/* this.audio.addEventListener('pause', this.handlePause.bind(this)) */

		/* render dom */
		this.render()
	}
	/* handle events and set attributes of the player accordingly */
	handleLoadedData() {
		this.setAttribute('loaded', true)
		this.removeAttribute('disabled')
	}
	handlePlay() {
		this.setAttribute('play', true)
		this.removeAttribute('pause')
		this.removeAttribute('error')
	}
	handlePause() {
		this.setAttribute('pause', true)
		this.removeAttribute('play')
		this.resetLottie()
		this.resetFaPlay()
	}
	handleEnded() {
		this.removeAttribute('pause', false)
		this.removeAttribute('play', false)
		this.resetLottie()
		this.resetFaPlay()

	}
	handleError() {
		this.setAttribute('error', true)
		this.setAttribute('pause', true)
		this.removeAttribute('play', false)
		this.resetLottie()
		this.resetFaPlay()
	}

	resetLottie() {
		if (this.$lottie) {
			this.$lottie.stop()
		}
	}
	resetFaPlay() {
		if (this.faPlay && this.faPlay.length) {
			const $faIcon = this.$icon.querySelector('i')
			if ($faIcon) {
				this.faPlay.forEach(faClass => {
					$faIcon.classList.remove(faClass)
				})
			}
		}
	}
	addFaPlay() {
		if (this.faPlay && this.faPlay.length) {
			const $faIcon = this.$icon.querySelector('i')
			if ($faIcon) {
				this.faPlay.forEach(faClass => {
					$faIcon.classList.add(faClass)
				})
			}
		}
	}

	render() {
		if (!this.$icon.assignedElements().length) {
			this.$icon.innerHTML = ''
			if (this.symbol) {
				const $materialSymbol = document.createElement('i')
				$materialSymbol.classList.add(`material-symbols-${this.symbolType}`)
				$materialSymbol.innerText = this.symbol
				this.$icon.append($materialSymbol)
			} else if (this.fa && this.fa.length) {
				const $fontAwesomeIcon = document.createElement('i')
				this.fa.forEach(faClass => {
					$fontAwesomeIcon.classList.add(faClass)
				})
				this.$icon.append($fontAwesomeIcon)
			}
		}

		if (!this.$text.assignedElements().length && this.text) {
			this.$text.innerHTML = ''
			const $text = document.createElement('span')
			$text.innerText = this.text
			this.$text.append($text)
		}
	}

	onClick(event) {
		event.preventDefault()

		/* play the audio file, we're here for that no?! */
		this.playAudio()

		/* add classes for the font-awesome animations */
		this.addFaPlay()

		/* trigger lottie player, if any slotted children */
		this.$icon.assignedElements().every($element => {
			if ($element.nodeName === 'LOTTIE-PLAYER') {
				this.$lottie = $element
			} else {
				this.$lottie = $element.querySelector('lottie-player')
			}

			if (this.$lottie) {
				this.$lottie.play()
				return false
			}
		})

		/* toggle details element, if any slotted children */
		this.$text.assignedElements().every($element => {
			let $details
			if ($element.nodeName === 'DETAILS') {
				$details = $element
			} else {
				$details = $element.querySelector('details')
			}
			if ($details) {
				$details.open = !$details.open
				return false
			}
		})
	}

	playAudio() {
		this.audio.currentTime = 0
		this.audio.play()
	}
}
